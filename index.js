'use strict';

const app = new PIXI.Application(1000, 800, {backgroundColor : 0x1099bb});
document.body.appendChild(app.view);

const rendererWidth = app.renderer.width;
const rendererHeight = app.renderer.height;

// everything is global for quick demo
let wind;
let windDisplace;
let stopEffect = true;
let turnSpeed = 1;
let scaleSpeed = 0.1;
let ken;
let kenFrames;
let isFreeMode = false;


// generate frames for animation from spritesheet
function generateFrames(assetList) {
  const frames = [];

  for (let i = 0; i < assetList.length; i += 1) {
    frames.push(PIXI.Texture.fromFrame(assetList[i]));
  }

  return frames;
}


const loader = new PIXI.loaders.Loader();
loader.add('wind.png');
loader.add('ground.png');
loader.add('ken', 'ken.json');
loader.load((loader, resources) => {
  const frameInfo = Object.keys(resources.ken.data.frames);
  kenFrames = generateFrames(frameInfo);

  start();
});

function playAction(pos) {
  // move effect position to mouse position
  windDisplace.position.x = wind.position.x = pos.x;
  windDisplace.position.y = wind.position.y = pos.y;

  // reset to initial value
  wind.scale.x = wind.scale.y = 0.01;
  scaleSpeed = 0.1;
  wind.alpha = 1;

  // play effect
  stopEffect = false;
}

function animate(delta) {
  if (stopEffect) {
    return;
  }

  // spin the wind
  windDisplace.rotation = wind.rotation += turnSpeed * delta;

  // scale the wind as it originated from a point
  scaleSpeed *= 1.5;
  windDisplace.scale.x = wind.scale.x += scaleSpeed * delta;
  // generate random wind shape for fun
  windDisplace.scale.y = wind.scale.y = wind.scale.x * delta * Math.random() * 2;

  // fade out
  wind.alpha -= 0.1 * delta;

  // stop animation when wind is out of camera scope
  if (wind.scale.y > 100) {
    stopEffect = true;
  }
}

function start() {
  const container = new PIXI.Container();
  container.interactive = true;
  container.on('pointerdown', (e) => {
    if (isFreeMode) {
      playAction(e.data.global);
    }

    ken.playHitAnim = true;
    ken.animationSpeed = 0.2;
    ken.gotoAndPlay(0);
  });

  // the ground
  const ground = PIXI.Sprite.fromImage('ground.png');
  const groundAspectRatio = ground.width / ground.height;
  ground.height = rendererHeight + 100;
  ground.width = ground.height * groundAspectRatio;
  ground.position.x = (rendererWidth - ground.width) / 2;
  ground.position.y = -10;

  container.addChild(ground);

  // ken
  ken = new PIXI.extras.AnimatedSprite(kenFrames);
  ken.pivot.x = ken.width / 2;
  ken.pivot.y = ken.height;
  ken.position.x = rendererWidth * 0.6;
  ken.position.y = rendererHeight / 2;
  ken.scale.x = ken.scale.y = 1.5;
  ken.animationSpeed = 0.1;
  ken.gotoAndPlay(6);

  ken.onFrameChange = (frameId) => {
    if (!ken.playHitAnim && frameId < 6) {
      // play idle animation which start from frame 6
      ken.gotoAndPlay(6);
      return;
    }

    if (frameId === 4) {
      ken.gotoAndPlay(6);
      ken.animationSpeed = 0.1;

      if (!isFreeMode) {
        playAction({
          x: ken.position.x - 30,
          y: ken.position.y - 30,
        });
      }
    }

    if (frameId >= 6) {
      ken.playHitAnim = false;
    }
  };


  container.addChild(ken);


  // filter assets
  windDisplace = PIXI.Sprite.fromImage('wind.png');
  windDisplace.pivot.x = windDisplace.width / 2;
  windDisplace.pivot.y = windDisplace.height / 2;
  windDisplace.alpha = 0;

  const filter = new PIXI.filters.DisplacementFilter(windDisplace);
  container.filters = [filter];

  container.addChild(windDisplace);

  // the wind itself
  wind = PIXI.Sprite.fromImage('wind.png');
  wind.pivot.x = wind.width / 2;
  wind.pivot.y = wind.height / 2;
  wind.alpha = 0;

  container.addChild(wind);

  app.stage.addChild(container);

  // Listen for animate update
  app.ticker.add(animate);
}


window.onload = function () {
  // mode shifter
  const textContentDom = document.getElementById('textContent');
  textContentDom.textContent = isFreeMode ? 'Free mode' : 'Character mode';

  const changeModeBtnDom = document.getElementById('changeModeBtn');
  changeModeBtnDom.onclick = function () {
    isFreeMode = !isFreeMode;
    textContentDom.textContent = isFreeMode ? 'Free mode' : 'Character mode';
  };
};
